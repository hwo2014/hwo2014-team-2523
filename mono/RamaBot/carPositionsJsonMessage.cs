﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RamaBot
{
    public class carPositionsJsonMessage
    {
        public string msgType { get; set; }
        public string gameId { get; set; }
        public List<CarPosition> data { get; set; }
    }


    public class CarPosition
    {
        public Id id { get; set; }
        public decimal angle { get; set; }
        public PiecePosition piecePosition { get; set; }
    }

    public class Id
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class PiecePosition
    {
        public int pieceIndex { get; set; }
        public decimal inPieceDistance { get; set; }
        public int lap { get; set; }
        public PiecePositionLane lane { get; set; }

    }

    public class PiecePositionLane
    {
        public int startLaneIndex { get; set; }
        public int endLaneIndex { get; set; }
    }
}
