﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RamaBot
{
    public class GameInitJsonMessage
    {
        public string msgType { get; set; }
        public GameInitData data { get; set; }
    }

    public class GameInitData
    {
        public GameInitRaceData race { get; set; }
    }

    public class GameInitRaceData
    {
        public GameInitTrackData track { get; set; }
        public List<GameInitCarInfo> cars { get; set; }
        public GameInitRaceSessionInfo raceSession { get; set; }
    }

    public class GameInitRaceSessionInfo
    {
        public int laps { get; set; }
        public int maxLapTimeMs { get; set; }
        public bool? quickRace { get; set; }
    }

    public class GameInitTrackData
    {
        public string trackId { get; set; }
        public string name { get; set; }
        public List<Piece> pieces { get; set; }
        public List<Lane> lanes { get; set; }
    }

    public class Piece
    {
        public decimal? length { get; set; }
        public decimal? angle { get; set; }
        public decimal? radius { get; set; }
        public bool? @switch { get; set; }
    }

    public class Lane
    {
        public decimal? distanceFromCenter { get; set; }
        public int index { get; set; }
    }

    public class GameInitCarInfo
    {
        public GameInitCarInfoId id { get; set; }
        public GameInitCarInfoDimensions dimensions { get; set; }
    }

    public class GameInitCarInfoId
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class GameInitCarInfoDimensions
    {
        public decimal? length { get; set; }
        public decimal? width { get; set; }
        public decimal? guideFlagPosition { get; set; }
    }
}
