using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using RamaBot;


public class Bot {

    public GameInitTrackData track;

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;
        object raceInit;
        //CarPositionsData testi;
   

		send(join);

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
                    //JArray data = msg.data as JArray;
                    //Console.WriteLine(data.ToString());
                    var carPositionsMessage = JsonConvert.DeserializeObject<carPositionsJsonMessage>(line);
                    //testi = carPositionsMessage.data;
					send(new Throttle(0.5));
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
                    //Track t = msg.GetTrackData();
                    var gameInitMessage = JsonConvert.DeserializeObject<GameInitJsonMessage>(line);
                    track = gameInitMessage.data.race.track;
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }

    //Datat taulukoihin
    //public Track GetTrackData()
    //{
    //    //JArray jData = data as JArray;
    //    JObject jObject = data as JObject;



    //    //JToken token = jData.SelectToken("/data/race/track/pieces");

    //    //Console.WriteLine(token.ToString());
    //    //JArray obj = JArray.Parse(data.ToString()) as JArray;

    //    //return null;
       

    //    //var searches = from p in obj.Children() select p;
    //    //foreach (JArray search in searches)
    //    //{
    //    //    Console.WriteLine("Search type: {0}; parameters:", (string)search.First);
    //    //    foreach (JProperty child in search.Last.Children())
    //    //    {
    //    //        Console.WriteLine("  {0}: {1}", child.Name, (string)child.Value);
    //    //    } // end foreach
    //    //} // end foreach

    //    //Console.Write("Press any key to continue . . . ");
    //    //Console.ReadKey(true);

    //}
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

//class Track
//{
//    public List<Piece> Pieces { get; set; }
//}

//class Piece
//{
//    public int Length { get; set; }
//    public bool Switch { get; set; }
//    public int Radius { get; set; }
//    public double Angle { get; set; }
//}


